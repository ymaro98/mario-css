// JavaScript Document
console.log("Wagwan world!");
console.log("-------------");

let buttons = document.querySelectorAll('button');
buttons.forEach(button => {       
    button.addEventListener('click' , checkButton, false);
});

function playSound(sfx, rate = 1){
    let sound = new Audio(`sounds/${sfx}.mp3`);
    sound.playbackRate = rate;
    sound.play();
}

function checkButton(e){

    const animationCheck = document.querySelector('.mario');
    animationCheck.addEventListener("animationstart", () =>{

        buttons.forEach(button => {
            
            button.disabled = true; 
        });
    })
    animationCheck.addEventListener("animationend", () =>{

        for (var i = 0 ; i < buttons.length; i++) {
            buttons[i].disabled = false; 
        }
    })

    const action = e.target.innerText;
    if(action == 'Blink'){

        playSound('blink', 2)
        animationCheck.classList.add("blink"); 
        animationCheck.onanimationend = () => {
            animationCheck.classList.remove("blink");
        };

    } else if(action == 'Yahoo'){

        playSound('yahoo', 1.2)

        animationCheck.classList.add("yahoo"); 
        animationCheck.onanimationend = () => {
            animationCheck.classList.remove("yahoo");
        };

    } else if(action == 'Waaa'){

        playSound('waaa', 2)

        animationCheck.classList.add("waaa"); 
        animationCheck.onanimationend = (e) => {
            animationCheck.classList.remove("waaa");
        };
    } else if(action == "Who dis?"){

        playSound('its-me-mario');

        animationCheck.classList.add("who-dis"); 
        animationCheck.onanimationend = (e) => {
            if(e.animationName == 'who_dis_mouth'){
                animationCheck.classList.remove("who-dis");
            }
        };
    } else if(action == 'Licky lick'){

        playSound('licking');

        animationCheck.classList.add("licky"); 
        animationCheck.onanimationend = (e) => {

            if(e.animationName == 'licky_tong'){
                animationCheck.classList.remove("licky");
            }
        };
    } else if(action == 'Kono Mario Da'){
        
        playSound('dio');
        
        animationCheck.classList.add("dio"); 
        animationCheck.onanimationend = (e) => {

            if(e.animationName == 'dio_zoom'){
                animationCheck.classList.remove("dio");
            }
        };
    } else if(action == "Scary"){
        const animations = [
            'scary_eyecolor',
            'move_scary_eyes',
            'scary_bg',
        ];

        animationCheck.classList.add("scary", animations[0]);
                  
        animationCheck.addEventListener("animationend", function(e){
            if(animations.includes(e.animationName)){
                let key = animations.indexOf(e.animationName);
                const nextKey = key + 1;
                setAnimationClass([animations[key], animations[nextKey]]);
            }

            if(e.animationName == 'scary_eyecolor'){
                playSound('cracking', 1.6);
            }

            if(e.animationName == 'move_scary_eyes'){
                playSound('scary', 1.3);                
            }
        }, false);

        let setAnimationClass = (animation) => {

            animationCheck.classList.remove(animation[0]);
            if(typeof animation[1] != 'undefined'){ 
                animationCheck.classList.add(animation[1])
            } else {
                animationCheck.classList.remove('scary');
            };
        }
    }
}